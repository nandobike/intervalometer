/*
Nando shutter
 */

// include the library code:
#include <LiquidCrystal.h>

// Infrared library
#include <IRremote.h>
IRsend irsend;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 9, 8, 7, 6);

int taken=0; //how many pictures are already taken
int exposure=45; //how many seconds open
int rest=10; //how long to rest
int time_remaining;

signed long last_pic=0;
signed long time_now;

boolean expo=false;

int i=0;




void setup() {
  
  lcd.begin(16, 2);    // set up the number of columns and rows on the LCD

  // set up the switch pin as an input
  //pinMode(switchPin, INPUT);

  lcd.clear();

  lcd.print("Starting at 3"); //Next in, how many taken, exposure/break
  lcd.setCursor(0, 1);
  // print to the second line
  lcd.print("0..");
  delay(1000);
lcd.print("1..");
  delay(1000);
  lcd.print("2..");
  delay(1000);
  lcd.print("3!");
  delay(1000);

  lcd.clear();
  lcd.setCursor(0, 0);


  
  
  lcd.print("Remn Tkn Exp/Rst"); //Next in, how many taken, exposure/break


  lcd.setCursor(0, 1);
  // print to the second line



time_remaining=exposure*1000-millis()+last_pic;

  

}





void loop() {
   // print the number of pictures taken
  lcd.setCursor(5, 1);
  lcd.print(taken);

     // print exposure
  lcd.setCursor(9, 1);
  lcd.print(exposure);

  // slash
  lcd.setCursor(12, 1);
  lcd.print("/");

  // print rest
  lcd.setCursor(13, 1);
  lcd.print(rest);




  
  
  if (!expo) { //this is resting
    lcd.setCursor(0, 0);
    lcd.print("Rest Tkn Exp/Rst"); //Next in, how many taken, exposure/break
    lcd.setCursor(0, 1);
    lcd.print("    ");
    for (i=1;i<=rest;i++){
      delay(1000);
      lcd.setCursor(0, 1);
      lcd.print(i);
    }
    shutter(); //start taking the picture
    expo=true;
  }

  if (expo) { //this is exposing
    lcd.setCursor(0, 0);
    lcd.print("Expo Tkn Exp/Rst"); //Next in, how many taken, exposure/break
    lcd.setCursor(0, 1);
    lcd.print("    ");
    for (i=1;i<=exposure;i++){
      delay(1000);
      lcd.setCursor(0, 1);
      lcd.print(i);
    }
    shutter(); //stop taking the picture
    taken+=1;
    expo=false;
  }

  











  
}

void shutter(){   //take a picture or stop the pic
  for (int i = 0; i < 3; i++) {
    irsend.sendSony(0xB4B8F, 20); //send shutter
    delay(40);
  }
  //delay(1000); //5 second delay between each signal burst
}
