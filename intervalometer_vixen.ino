/*
  DigitalReadSerial

  Reads a digital input on pin 2, prints the result to the Serial Monitor

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/DigitalReadSerial
*/

// Infrared library
#include <IRremote.h>
IRsend irsend;


// digital pin 2 has a pushbutton attached to it. Give it a name:
int pushButton = 2;
int previousButton;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the pushbutton's pin an input:
  pinMode(pushButton, INPUT);
  previousButton = digitalRead(pushButton);
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  
  // read the input pin:
  int buttonState = digitalRead(pushButton);
  // print out the state of the button:
  Serial.println(buttonState);
  if (buttonState == 0){
    digitalWrite(LED_BUILTIN, HIGH);
  } else {
    digitalWrite(LED_BUILTIN, LOW);
  }
  
  if (buttonState != previousButton){
    Serial.println("CHANGE");
    shutter();
  }
  delay(30);        // delay in between reads for stability
  previousButton = buttonState;

  
}



void shutter(){   //take a picture or stop the pic
  for (int i = 0; i < 3; i++) {
    irsend.sendSony(0xB4B8F, 20); //send shutter
    Serial.println("SHUTTER!");
    delay(40);
  }
  //delay(1000); //5 second delay between each signal burst
}